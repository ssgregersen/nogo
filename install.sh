#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

NOGO_INSTALL_PATH="${NOGO_INSTALL_PATH:-/opt/nogo}"

rm -rf "/opt/nogo"

echo "Installing nogo in '${NOGO_INSTALL_PATH}'!"

mkdir -p "${NOGO_INSTALL_PATH}"
wget -qO - "https://bitbucket.org/ssgregersen/nogo/get/master.tar.gz" \
    | tar -xzf - -C "${NOGO_INSTALL_PATH}" --strip-components=2 \
        --wildcards "ssgregersen-nogo-*/nogo/*"

chmod +x "${NOGO_INSTALL_PATH}/nogo.sh"

mkdir -p "/opt/bin"
rm -rf "/opt/bin/nogo"
ln -s "${NOGO_INSTALL_PATH}/nogo.sh" "/opt/bin/nogo"

cat > "/etc/profile.d/nogo.sh" <<EOF
# Expand the \$PATH to include /opt/bin, which is what nogo uses
[[ ":\$PATH:" != *":/opt/bin:"* ]] && export PATH="/opt/bin:\${PATH}"
EOF

echo "Done. Restart bash and initiate using: nogo init"
