### What is this repository for? ###

```
nogo - go-inspired basic script manager

# Usage: #

nogo [-h|--help] operator [options] [git-repository]

# Possible operators: #

- init      Sets up the src, bin, and config directories for nogo.

- get       Clone the source repository into the nogo src directory

- install   Will get if source is not available, then run

            > $NOGO_SRC_PATH/git-repository-path/install.sh

            FALLBACK:
            If no install.sh script exists, will copy the first
            file (preference to repo-basename[.sh]).

- update    Will get if source is not available, then run

            > $NOGO_SRC_PATH/git-repository-path/update.sh

            FALLBACK:
            If no update.sh script exists, will run

            > nogo remove repo && nogo install repo

- remove    Will get if source is not available, then run

            > $NOGO_SRC_PATH/git-repository-path/remove.sh

            FALLBACK:
            If no remove.sh script exists, will remove source and
            binary based of the repo name.

- path      Prints the nogo formatted path from the git-repository URL

- clean     Traverses the NOGO_SRC_PATH and removes empty directories.
            This is performed automatically after the operations:
            install, update, and remove.

# Options: #

-h|--help           Show this help message.

-p|--print-only     Only print the operation(s)

-t|--tmp-src        Only temporary source files---removes the source
                    after use.

-n|--no-clean       Disables the automatic clean after the operations:
                    install, update, and remove.
```

# Depends on git
# Single line install:
```
> curl -s "https://bitbucket.org/ssgregersen/nogo/raw/master/install.sh" | sudo bash
```

