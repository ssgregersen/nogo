# We assume the binary is either:
# - A single file of the same name as the repo basename
# - The only present file

nogo_basename="$(basename ${nogo_path})"
nogo_file_num="$(ls -1 "${NOGO_SRC_PATH}/${nogo_path}" | wc -l)"
nogo_first_file="$(ls "${NOGO_SRC_PATH}/${nogo_path}" | head -1)"
if [ -f "${NOGO_SRC_PATH}/${nogo_path}/${nogo_basename}" ]; then
    if [ -f "${NOGO_BIN_PATH}/${nogo_basename}" ]; then
        rm "${NOGO_BIN_PATH}/${nogo_basename}"
    fi
elif [ "${nogo_file_num}" -eq 1]; then
    if [ -f "${NOGO_BIN_PATH}/${nogo_first_file}" ]; then
        rm "${NOGO_BIN_PATH}/${nogo_first_file}"
    fi
fi

# When here, the binary is uninstalled.

rm -rf "${NOGO_SRC_PATH}/${nogo_path}"
