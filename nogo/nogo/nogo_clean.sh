
find "${NOGO_SRC_PATH}" -type d -empty -delete # prune empty directories
mkdir -p "${NOGO_SRC_PATH}" # keep the source directory, even if empty.
