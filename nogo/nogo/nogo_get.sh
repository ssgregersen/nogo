if [ -d "${NOGO_SRC_PATH}/${nogo_path}" ]; then
    echo "A repository already exits and will not be changed."
    echo "Remove using: nogo remove ${nogo_path}."
else
    mkdir -p "${NOGO_SRC_PATH}/${nogo_path}"
    git clone "${nogo_repository}" "${NOGO_SRC_PATH}/${nogo_path}"
fi
