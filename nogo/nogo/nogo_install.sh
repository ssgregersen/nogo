# We assume the binary is either:
# - A single file of the same name as the repo basename
# - The only present file

nogo_basename="$(basename ${nogo_path})"
nogo_file_num="$(ls -1 "${NOGO_SRC_PATH}/${nogo_path}" | wc -l)"
nogo_first_file="$(ls "${NOGO_SRC_PATH}/${nogo_path}" | head -1)"
if [ -f "${NOGO_SRC_PATH}/${nogo_path}/${nogo_basename}" ]; then
    if [ -f "${NOGO_BIN_PATH}/${nogo_basename}" ]; then
        echo "A binary already exits. Remove using: nogo remove ${nogo_path}."
        exit 1
    fi
    nogo_bin_src="${NOGO_SRC_PATH}/${nogo_path}/${nogo_basename}"
    nogo_bin_dst="${NOGO_BIN_PATH}/${nogo_basename}"
elif [ "${nogo_file_num}" -eq 1]; then
    if [ -f "${NOGO_BIN_PATH}/${nogo_first_file}" ]; then
        echo "A binary already exits. Remove using: nogo remove ${nogo_path}."
        exit 1
    fi
    nogo_bin_src="${NOGO_SRC_PATH}/${nogo_path}/${nogo_first_file}"
    nogo_bin_dst="${NOGO_BIN_PATH}/${nogo_first_file}"
else
    echo "A binary could be identified from the source directory."
    exit 1
fi

# When here, the binary is valid and *not* installed.

echo "${nogo_bin_src}"
echo "${nogo_bin_dst}"

cp "${nogo_bin_src}" "${nogo_bin_dst}"
