#!/usr/bin/env bash

NOGO_INSTALL_PATH="${NOGO_INSTALL_PATH:-/opt/nogo}"
NOGO_CONFIG_PATH="${NOGO_CONFIG_PATH:--${HOME}/.config/nogo/}"

usage() {
    sed 's/^    //' <<EOF
    nogo - go-inspired basic script manager

    usage:

        > nogo [-h|--help] operator [options] git-repository

    Possible operators:

        - init      Sets up the src, bin, and config directories for nogo.

        - get       Clone the source repository into the nogo src directory

        - install   Will get if source is not available, then run

                    > $NOGO_SRC_PATH/git-repository-path/install.sh

                    FALLBACK:
                    If no install.sh script exists, will copy the first
                    file (preference to repo-basename[.sh]).

        - update    Will get if source is not available, then run

                    > $NOGO_SRC_PATH/git-repository-path/update.sh

                    FALLBACK:
                    If no update.sh script exists, will run

		            > nogo remove repo && nogo install repo

        - remove    Will get if source is not available, then run

                    > $NOGO_SRC_PATH/git-repository-path/remove.sh

                    FALLBACK:
                    If no remove.sh script exists, will remove source and
		            binary based of the repo name.

        - path      Prints the nogo formatted path from the git-repository URL

        - clean     Traverses the NOGO_SRC_PATH and removes empty directories.
                    This is performed automatically after the operations:
                    install, update, and remove.

    Options:

        -h|--help           Show this help message.

        -p|--print-only     Only print the operation(s)

        -t|--tmp-src        Only temporary source files---removes the source
                            after use.

        -n|--no-clean       Disables the automatic clean after the operations:
                            install, update, and remove.

    More information available at:

        https://bitbucket.org/ssgregersen/nogo/

EOF
}

nogo_main() {
    case ${nogo_operator} in
    init|clean)
        nogo_no_repo_check # These operations do not support a repository
        case ${nogo_operator} in
        init) nogo_run_or_print_file "${NOGO_INSTALL_PATH}/nogo_init.sh" ;;
        clean) nogo_run_or_print_file "${NOGO_CONFIG_PATH}/nogo_clean.sh" ;;
        esac
        exit 1
    ;;
    get|install|update|remove|path)
        nogo_repo_check # These operations need a repository
        nogo_config_paths
        nogo_tmp_mk # If tmp src, then make the change here
        nogo_print_config
        case ${nogo_operator} in
        get) nogo_run_or_print_file "${NOGO_CONFIG_PATH}/nogo_get.sh" ;;
        path) nogo_run_or_print_file "${NOGO_CONFIG_PATH}/nogo_path.sh" ;;
        install|update|remove)
            case ${nogo_operator} in
            install) nogo_install ;;
            update) nogo_update ;;
            remove) nogo_remove ;;
            esac
        ;;
        esac
        nogo_tmp_rm # If tmp src, then remove
    ;;
    esac
}

nogo_install(){
    source "${NOGO_CONFIG_PATH}/nogo_get.sh"
    if [ -f "$NOGO_SRC_PATH/${nogo_path}/install.sh" ]; then
        nogo_run_or_print_file "$NOGO_SRC_PATH/${nogo_path}/install.sh"
    else
        nogo_run_or_print_file "${NOGO_CONFIG_PATH}/nogo_install.sh"
    fi
}

nogo_update() {
    source "${NOGO_CONFIG_PATH}/nogo_get.sh"
    if [ -f "$NOGO_SRC_PATH/${nogo_path}/update.sh" ]; then
        nogo_run_or_print_file "$NOGO_SRC_PATH/${nogo_path}/update.sh"
    else
        nogo_run_or_print_file "${NOGO_CONFIG_PATH}/nogo_update.sh"
    fi
}

nogo_remove() {
    source "${NOGO_CONFIG_PATH}/nogo_get.sh"
    if [ -f "$NOGO_SRC_PATH/${nogo_path}/remove.sh" ]; then
        nogo_run_or_print_file "$NOGO_SRC_PATH/${nogo_path}/remove.sh"
    else
        nogo_run_or_print_file "${NOGO_CONFIG_PATH}/nogo_remove.sh"
    fi
    if [ "$nogo_no_clean" -ne 1 ]; then
        source "${NOGO_CONFIG_PATH}/nogo_clean.sh"
    fi
}


nogo_config_paths() {
    if [ -f ${NOGO_CONFIG_PATH}/config ]; then
        source ${NOGO_CONFIG_PATH}/config
        NOGO_SRC_PATH="${NOGO_SRC_PATH:-${HOME}/src}"
        NOGO_BIN_PATH="${NOGO_BIN_PATH:-${HOME}/bin}"
    else
        echo "nogo is not ready, run: nogo init"
        exit 1
    fi

    nogo_path="$(source "${NOGO_CONFIG_PATH}/nogo_path.sh")"
}


nogo_print_config() {
    if [ "$nogo_print_only" -ne 0 ]; then
        echo "\$NOGO_SRC_PATH=\"$NOGO_SRC_PATH\""
        echo "\$NOGO_BIN_PATH=\"$NOGO_BIN_PATH\""
        echo "\$NOGO_CONFIG_PATH=\"${NOGO_CONFIG_PATH}\""
        echo # blank line
        echo "\$nogo_repository=\"$nogo_repository\""
        echo "\${nogo_path}=\"${nogo_path}\""
        echo # blank line
    fi
}

nogo_tmp_mk() {
    if [ "$nogo_tmp_src" -ne 0 ]; then
        nogo_tmp_dir=$(mktemp -d)
        NOGO_SRC_PATH="$nogo_tmp_dir"
    else
        nogo_tmp_dir=0
    fi
}

nogo_tmp_rm() {
    if [ -d "$nogo_tmp_dir" ]; then
        rm -rf $nogo_tmp_dir
        if [ "$nogo_print_only" -ne 0 ]; then
            echo "rm -rf $nogo_tmp_dir"
        fi
    fi
}

nogo_run_or_print_file() {
    [ "$nogo_print_only" -ne 0 ] && cat $1 || source $1
    [ "$nogo_print_only" -ne 0 ] && echo # end in blank line
}

nogo_repo_check() {
    if [ $nogo_repository -eq 0 ]; then
        echo "Missing repository url."
        usage ; exit 1
    fi
}

nogo_no_repo_check() {
    if [ $nogo_repository -nq 0 ]; then
        echo "The ${nogo_operator} operator takes no repository."
        usage ; exit 1
    fi
}

##############################################################################
# The program:

# Process all arguments (for now only supports the short forms of options)

nogo_operator="$1"
shift 1
case "${nogo_operator}" in
    init|get|install|update|remove|path|clean) ;;
    -h|--help) usage; exit 1 ;;
    *) echo "Unknown operator"; usage; exit 1 ;;
esac

nogo_print_only=0
nogo_tmp_src=0
nogo_no_clean=0
while getopts "hptn" opt; do
    case "${opt}" in
        h) usage ; exit 1 ;;
        p) nogo_print_only=1 ;;    # Only print the operation script(s)
        t) nogo_tmp_src=1 ;;    # No source files
        n) nogo_no_clean=1 ;;    # No source files
    esac
done
shift $((OPTIND-1))

if [ $# -eq 0 ]; then
    nogo_repository=0
else
    nogo_repository="$1"
    shift 1
fi

# run the main function
nogo_main
